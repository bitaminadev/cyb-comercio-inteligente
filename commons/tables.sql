/* TABLA PRIVILEGIOS */
    CREATE TABLE IF NOT EXISTS `Privilegios`(
        `ID_privilegio` INT NOT NULL AUTO_INCREMENT,
        `Nombre` varchar(25),
        `Descripcion` text NOT NULL,
        PRIMARY KEY(`ID_privilegio`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    /* TABLA ROLES */
    CREATE TABLE IF NOT EXISTS `Roles`(
        `ID_rol` INT NOT NULL AUTO_INCREMENT,
        `Nombre` varchar(25),
        `Descripcion` text NOT NULL,
        PRIMARY KEY (`ID_rol`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    /* TABLA ROLES_PRIVILEGIOS */
    CREATE TABLE IF NOT EXISTS `Roles_Privilegios`(
        `ID_privilege` INT NOT NULL,
        `ID_rol` INT NOT NULL,
        KEY `ID_privilege` (`ID_privilege`),
        KEY `ID_rol` (`ID_rol`),
        PRIMARY KEY (ID_privilege, ID_rol)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    /* TABLA USUARIOS */ 
    CREATE TABLE IF NOT EXISTS `Usuarios`(
        `id` INT NOT NULL AUTO_INCREMENT,
        `password` text NOT NULL,
        `first_name` varchar(50) NOT NULL,
        `last_name` varchar(50) NOT NULL,
        `email` varchar(50) NOT NULL,
        `tel` varchar(15) NOT NULL,
        PRIMARY KEY (`id`)
    )ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    /* TABLA ROLES_USUARIOS */
    CREATE TABLE IF NOT EXISTS `Roles_Usuarios`(
        `ID_rol` INT NOT NULL,
        `ID_usuario` INT NOT NULL,
        KEY `ID_rol` (`ID_rol`),
        KEY `ID_usuario` (`ID_usuario`),
        PRIMARY KEY (ID_rol, ID_usuario)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    
    /* CONSTRAINTS */
    ALTER TABLE `Roles_Privilegios`
        ADD CONSTRAINT `rls_priv_fk1` FOREIGN KEY (`ID_rol`) REFERENCES `Roles` (`ID_rol`),
        ADD CONSTRAINT `priv_rls_fk2` FOREIGN KEY (`ID_privilegio`) REFERENCES `Privilegios` (`ID_privilegio`);

    ALTER TABLE `Roles_Usuarios`
        ADD CONSTRAINT `rls_usr_fk1` FOREIGN KEY (`ID_rol`) REFERENCES `Roles` (`ID_rol`),
        ADD CONSTRAINT `usr_rls_fk2` FOREIGN KEY (`ID_usuario`) REFERENCES `Usuarios` (`ID_usuario`);